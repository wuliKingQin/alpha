一个Android启动优化的库，来源于阿里的alpha
#### 引入配置

1.项目根build.gradle配置

```
allprojects {
    repositories {
	...
	maven { url 'https://jitpack.io' }
    }
}
```
2. app目录里的build.gradle配置

```
dependencies {
    ...
    implementation 'com.gitee.wuliKingQin:alpha:1.0.0'
}
```
